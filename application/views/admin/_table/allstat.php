<div class="col-md-5">
    <div class="main-card mb-3 card">
        <div class="card-header">Node B Status
        </div>
        <div class="table-responsive" id="table-container">
            <table class="table table-striped" id="allstat">
                <thead>
                <tr>
                    <th class="text-center">Regional</th>
                    <th class="text-center"><div class="badge badge-success">UP</div></th>
                    <th class="text-center"><div class="badge badge-danger">Down</div></th>
                    <th class="text-center"><div class="badge badge-primary">Total</div></th>
                </tr>
                </thead>
                <tbody>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                </tbody>
                <tfoot>
                    <tr>
                        <th class="text-center">Total</th>
                        <th class="text-center"><div class="badge badge-success"></div></th>
                        <th class="text-center"><div class="badge-danger"></div></th>
                        <th class="text-center"><div class="badge badge-primary"></div></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>