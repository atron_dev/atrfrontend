<div class="col-md-7">
    <div class="main-card mb-3 card">
        <div class="card-header">NODE B OCCUPANCY OVERVIEW
        </div>
        <div class="table-responsive" id="table-containers">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-center">Regional</th>
                        <th class="text-center"><div class="badge badge-success">50%</div></th>
                        <th class="text-center"><div class="badge badge-warning">50%-70%</div></th>
                        <th class="text-center"><div class="badge badge-danger">>70%</div></th>
                        <th class="text-center"><div class="badge badge-primary">Total</div></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th class="text-center">Total</th>
                        <th class="text-center"><div class="badge badge-success"></div></th>
                        <th class="text-center"><div class="badge badge-warning"></div></th>
                        <th class="text-center"><div class="badge-danger"></div></th>
                        <th class="text-center"><div class="badge badge-primary"></div></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>