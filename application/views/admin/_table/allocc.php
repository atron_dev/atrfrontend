<div class="row">
    <div class="col-md-12">            
        <div class="main-card mb-3 card">
            <div class="card-header">All Data Occupancy
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tabs-eg-77">
                        <div class="widget-content p-0">
                            <table class="table table-striped" id="allocc">
                                <thead>
                                    <tr>
                                        <th class="text-center">TREG</th>
                                        <th class="text-center">Site ID</th>
                                        <th class="text-center">Site Name</th>
                                        <th class="text-center">Witel</th>
                                        <th class="text-center">IP</th>
                                        <th class="text-center">Bandwidth</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach ($info as $item) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $item['treg']; ?></td>
                                        <td class="text-center"><?php echo $item['site_id']; ?></td>
                                        <td class="text-center"><?php echo $item['site_name']; ?></td>
                                        <td class="text-center"><?php echo $item['witel']; ?></td>
                                        <td class="text-center"><?php echo $item['ip_ont']; ?></td>
                                        <td class="text-center"><?php echo $item['bw_current']; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>