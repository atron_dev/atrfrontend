<table class="align-middle mb-0 table table-borderless table-striped table-hover">
    <thead>
        <tr>
            <th class="text-center">Witel</th>
            <th class="text-center"><div class="badge badge-success">50%</div></th>
            <th class="text-center"><div class="badge badge-warning">50%-70%</div></th>
            <th class="text-center"><div class="badge badge-danger">>70%</div></th>
            <th class="text-center"><div class="badge badge-primary">Total</div></th>
        </tr>
    </thead>
    <?php 
        $total_up = 0;
        $total_middle = 0;
        $total_down = 0;
        $total=0;
        foreach ($occ as $item) { 
        $sub_total = $item->up + $item->middle + $item->down;
    ?>
    <tbody>
        <tr>
            <td class="text-center"><?php echo $item->city; ?></td>
            <td class="text-center"><a class="btn-transition btn btn-outline-success" href="<?php echo base_url().'index.php/utilities/witel_up/'.$item->city; ?>"><?php echo $item->up; ?></a></td>
            <td class="text-center"><a class="btn-transition btn btn-outline-warning" href="<?php echo base_url().'index.php/utilities/witel_middle/'.$item->city; ?>"><?php echo $item->middle; ?></a></td>
            <td class="text-center"><a class="btn-transition btn btn-outline-danger" href="<?php echo base_url().'index.php/utilities/witel_down/'.$item->city; ?>"><?php echo $item->down; ?></a></td>
            <td class="text-center"><a class="btn-transition btn btn-outline-primary" href="<?php echo base_url().'index.php/utilities/witel_total/'.$item->city; ?>"><?php echo $sub_total; ?></a></td>
        </tr>
    <?php
        $total_up += $item->total_up; 
        $total_middle += $item->total_middle;
        $total_down += $item->total_down;
        $total += $sub_total;
        } 
    ?>
    </tbody>
    <tfoot>
        <tr>
            <th class="text-center">Total</th>
            <th class="text-center"><div class="badge badge-success"><?php echo $total_up; ?></div></th>
            <th class="text-center"><div class="badge badge-warning"><?php echo $total_middle; ?></div></th>
            <th class="text-center"><div class="badge badge-danger"><?php echo $total_down; ?></div></th>
            <th class="text-center"><div class="badge badge-primary"><?php echo $total; ?></div></th>
        </tr>
    </tfoot>
</table>