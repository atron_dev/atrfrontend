<!DOCTYPE html>
<html>
<head>
    <?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body>

    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php $this->load->view("admin/_partials/navbar.php") ?>
        <div class="app-main">        
            <?php $this->load->view("admin/_partials/sidebar.php") ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <?php $this->load->view("admin/_partials/inner.php") ?>

                    <div class="row">
                        <?php $this->load->view("admin/_table/allstat.php") ?>
                        <?php $this->load->view("admin/_table/occ_reg.php") ?>
                    </div>                 
                    <?php $this->load->view("admin/_table/allocc.php") ?>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript" src="<?php echo base_url().'assets/scripts/jquery.js'?>"></script>
<script src="<?php echo base_url('assets/scripts/main.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/scripts/jquery.dataTables.js'?>"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('#allocc').DataTable( {});

    });

</script>
</body>
</html>