<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Utilities extends CI_Controller
{

    public function __construct() {

        parent::__construct();
        $this->load->model('Utilities_model');

    }

    public function index() {   
        
        $data['info'] = $this->Utilities_model->get_all();
        $this->load->view('admin/dashboard', $data);
    }
}